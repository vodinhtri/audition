﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextScore : MonoBehaviour
{
    private float initialScore;
    private float curScore;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        initialScore = float.Parse(GetComponent<TextMeshProUGUI>().text);
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        curScore = float.Parse(GetComponent<TextMeshProUGUI>().text);
        if(curScore != initialScore)
        {
            initialScore = curScore;
            animator.SetTrigger("scaling");
        }
        
    }
}
