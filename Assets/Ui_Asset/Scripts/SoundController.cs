﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    private AudioSource musicSource;

    public Image cooldown;

    public static SoundController instance = null;

    void Awake()
    {
        musicSource = GetComponent<AudioSource>();
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy (gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        musicSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(musicSource.clip.name);
        // Debug.Log(musicSource.time);
        // musicSource.clip.name;

        // cooldown.fillAmount = musicSource.time / musicSource.clip.length;
    }

    public float GetTimePercent()
    {
        return musicSource.time / musicSource.clip.length;
    }

    public string GetClipName()
    {
        return musicSource.clip.name;
    }
    public bool IsFinish(){
        if(GetTimePercent() >= 1) return true;
        return false;
    }
}
