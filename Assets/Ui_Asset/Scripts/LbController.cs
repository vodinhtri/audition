﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LbController : MonoBehaviour
{
    public static LbController instance;

    public GameObject textScore;
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public int topPlayer = 0;
    public float speed = 100f;

    private float player1Score;
    private float player2Score;
    private float player3Score;
    private bool isCanUpdate;

    private Vector3 firstPos = new Vector3(0.0f, 30.0f, 0.0f);
    private Vector3 middlePos = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 lastPos = new Vector3(0.0f, -30f, 0.0f);
    private GameObject maxScorePlayer;
    private GameObject minScorePlayer;
    private GameObject middleScorePlayer;
    

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy (gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        isCanUpdate = false;
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlayerScore();

        if(!isCanUpdate) return;
        maxScorePlayer.GetComponent<RectTransform>().anchoredPosition = 
            Vector3.MoveTowards(maxScorePlayer.GetComponent<RectTransform>().anchoredPosition, firstPos, speed * Time.deltaTime);
        minScorePlayer.GetComponent<RectTransform>().anchoredPosition =
            Vector3.MoveTowards(minScorePlayer.GetComponent<RectTransform>().anchoredPosition, lastPos, speed * Time.deltaTime);
        if(middleScorePlayer)
        {
            middleScorePlayer.GetComponent<RectTransform>().anchoredPosition = 
                Vector3.MoveTowards(middleScorePlayer.GetComponent<RectTransform>().anchoredPosition, middlePos, speed * Time.deltaTime);
        }
    }

    public void SetScoreCharacter(int idx, float score)
    {
        if(idx > -1 && idx < 3) isCanUpdate = true;
        switch(idx)
        {
            case 0:
                player1Score += score;
                break;
            case 1:
                player2Score += score;
                break;
            case 2:
                player3Score += score;
                break;
        }

        textScore.GetComponent<TextMeshProUGUI>().text = player1Score.ToString();

        maxScorePlayer = GetMaxScorePlayer();
        minScorePlayer = GetMinScorePlayer();
        middleScorePlayer = GetMiddleScorePlayer(
            minScorePlayer, 
            maxScorePlayer
        );
    }

    void UpdatePlayerScore()
    {
        player1.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = player1Score.ToString();
        player2.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = player2Score.ToString();
        player3.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = player3Score.ToString();
    }

    private GameObject GetMiddleScorePlayer(GameObject minScore, GameObject maxScore)
    {
        if(!GameObject.ReferenceEquals(player1, minScore) && !GameObject.ReferenceEquals(player1, maxScore))
            return player1;
        if(!GameObject.ReferenceEquals(player2, minScore) && !GameObject.ReferenceEquals(player2, maxScore))
            return player2;
        if(!GameObject.ReferenceEquals(player3, minScore) && !GameObject.ReferenceEquals(player3, maxScore))
            return player3;
        return null;
    }

    private GameObject GetMaxScorePlayer()
    {
        float result = Mathf.Max(
            player1Score,
            Mathf.Max(player2Score, player3Score)
        );

        

        if(player1Score == result)
        {
            AnimCharManager.instance.MoveCharacterUp(1);
            topPlayer = 1;
            return player1;
        }
            
        if(player2Score == result)
        {
            AnimCharManager.instance.MoveCharacterUp(2);
            topPlayer = 2;
            return player2;
        }
        if(player3Score == result)
        {
            AnimCharManager.instance.MoveCharacterUp(3);
            topPlayer = 3;
            return player3;
        }
        return null;
    }

    private GameObject GetMinScorePlayer()
    {
        float result = Mathf.Min(
            player1Score,
            Mathf.Min(player2Score, player3Score)
        );
        if(player1Score == result)
            return player1;
        if(player2Score == result)
            return player2;
        if(player3Score == result)
            return player3;
        return null;
    }

}
