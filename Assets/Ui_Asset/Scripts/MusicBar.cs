﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicBar : MonoBehaviour
{
    public Image fill;
    public GameObject nameSong1;
    public GameObject nameSong2;
    public int speedMove;

    private RectTransform rect1;
    private RectTransform rect2;
    private Vector2 initPos;

    // Start is called before the first frame update
    void Start()
    {
        rect1 = nameSong1.GetComponent<RectTransform>();
        rect2 = nameSong2.GetComponent<RectTransform>();
        initPos = rect2.anchoredPosition;

        nameSong1.GetComponent<Text>().text = SoundController.instance.GetClipName();
        nameSong2.GetComponent<Text>().text = SoundController.instance.GetClipName();
    }

    // Update is called once per frame
    void Update()
    {
        fill.fillAmount = SoundController.instance.GetTimePercent();

        rect1.anchoredPosition = new Vector2(
            rect1.anchoredPosition.x - speedMove * Time.deltaTime,
            rect1.anchoredPosition.y
        );
        rect2.anchoredPosition = new Vector2(
            rect2.anchoredPosition.x - speedMove * Time.deltaTime,
            rect2.anchoredPosition.y
        );

        if(rect1.anchoredPosition.x < -rect1.sizeDelta.x)
        {
            rect1.anchoredPosition = initPos;
        }
        if(rect2.anchoredPosition.x < -rect2.sizeDelta.x)
        {
            rect2.anchoredPosition = initPos;
        }
    }
}
