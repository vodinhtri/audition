﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public class RandomResultController : MonoBehaviour
{
    public string animChar = "char1";
    public BoardGameController boardGame;
    public Transform notificationBoard;

    private int waitForMiss;
    private bool runOnce;
    private float timeShowResult;
    private int finishResult;
    private bool waitForShowResult;
    // Start is called before the first frame update
    void Start()
    {
        this.waitForMiss = 0;
        this.waitForShowResult = false;
    }

    // Update is called once per frame
    void Update()
    {
        Slider beatSlider = this.boardGame.GetBeatSlide();
        // Debug.Log("Update(): " + beatSlider.value);
        if (beatSlider.value >= beatSlider.maxValue - 15)
        {
            this.runOnce = false;
        }

        if (!this.runOnce && beatSlider.value < beatSlider.minValue + 30 && this.IsTurnCanCaculate() && this.boardGame.GetStepPrepare() >= 2)
        {          
            // Debug.Log("LevelNumber: " + this.boardGame.GetLevelNumber() 
            //     + " - LevelCount: " + this.boardGame.GetLevelCount()
            //     + " - IsTurnCanCaculate: " + this.IsTurnCanCaculate());

            if (this.waitForMiss > 0)
            {
                // Debug.Log("ShowResult: WAITFORMISS");
                this.waitForMiss--;
            }
            else
            {
                this.finishResult = Random.Range(0, 5); //ResultType
                this.timeShowResult = this.TimeToShow(this.finishResult);
                this.runOnce = true;

                this.waitForShowResult = true;
            }
        }

        if (this.waitForShowResult && beatSlider.value > this.timeShowResult)
        {
            this.waitForShowResult = false;
            this.ShowResult(this.finishResult);
        }
    }

    void LateUpdate()
    {
        this.notificationBoard.LookAt(Camera.main.transform);
        this.notificationBoard.Rotate(0.0f, 180.0f, 0.0f);
    }

    public void ShowResult(int result)
    {
        // switch (result)
        // {
        //     case 0:
        //         Debug.Log("ShowResult: " + "WOW");
        //         this.waitForMiss = 1;
        //         break;
        //     case 1:
        //         Debug.Log("ShowResult: " + "GOOD");
        //         break;
        //     case 2:
        //         Debug.Log("ShowResult: " + "NICE");
        //         break;
        //     case 3:
        //         Debug.Log("ShowResult: " + "GREATE");
        //         break;
        //     case 4:
        //         Debug.Log("ShowResult: " + "PERFECT");
        //         break;
        // }
    
        int charIndex = 0;
        switch (this.animChar)
        {
            case "char2":
                charIndex = 1;
                break;
            case "char3":
                charIndex = 2;
                break;
        }

        switch (finishResult)
        {
            case 0:
                this.notificationBoard.GetChild(0).gameObject.SetActive(false);
                this.notificationBoard.GetChild(0).gameObject.SetActive(true);
                this.waitForMiss = 1;//hieubt
                AnimCharManager.instance.SetMiss(this.animChar);
                break;
            case 1:
                this.notificationBoard.GetChild(1).gameObject.SetActive(false);
                this.notificationBoard.GetChild(1).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(charIndex, 20.0f);
                break;
            case 2:
                this.notificationBoard.GetChild(2).gameObject.SetActive(false);
                this.notificationBoard.GetChild(2).gameObject.SetActive(true);                
                LbController.instance.SetScoreCharacter(charIndex, 50.0f);
                break;
            case 3:
                this.notificationBoard.GetChild(3).gameObject.SetActive(false);
                this.notificationBoard.GetChild(3).gameObject.SetActive(true);                
                LbController.instance.SetScoreCharacter(charIndex, 80.0f);
                break;
            case 4:
                this.notificationBoard.GetChild(4).gameObject.SetActive(false);
                this.notificationBoard.GetChild(4).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(charIndex, 100.0f);
                break;
        }

        if (finishResult != 0)
        {
            if (this.boardGame.GetLevelNumber() <= 9)
            {
                // this.boardGame.GetAnimationType();
                AnimCharManager.instance.SetDanceSingle(this.animChar, this.boardGame.GetAnimationType());
            }
            else
            {
                // AnimCharManager.instance.SetFinishDance();
                AnimCharManager.instance.SetFinishDanceSingle(this.animChar, this.boardGame.GetAnimationType() % 2);
            }
        }
    }

    public float TimeToShow(int result)
    {
        int rand01 = 0;
        switch (result)
        {
            case 0:
                rand01 = Random.Range(1, 3);
                if (rand01 == 1)
                {
                    return Random.Range(145, 150);
                }
                else
                {
                    return Random.Range(40, 75);
                }
            case 1:
                rand01 = Random.Range(0, 2);
                if (rand01 == 1)
                {
                    return Random.Range(130, 145);
                }
                else
                {
                    return Random.Range(75, 90);
                }
            case 2:
                rand01 = Random.Range(0, 2);
                if (rand01 == 1)
                {
                    return Random.Range(120, 130);
                }
                else
                {
                    return Random.Range(90, 100);
                }
            case 3:
                rand01 = Random.Range(0, 2);
                if (rand01 == 1)
                {
                    return Random.Range(113, 120);
                }
                else
                {
                    return Random.Range(100, 107);
                }
            default:
                return Random.Range(107, 113);
        }
    }

    public bool IsTurnCanCaculate()
    {if (this.boardGame.GetLevelNumber() < 6)
        {
            return true;
        } 
        else if (this.boardGame.GetLevelNumber() < 10)
        {
            if (this.boardGame.GetLevelCount() % 2 == 0)
            {
                return true;
            }
        }
        else if (this.boardGame.GetLevelNumber() == 10)
        {
            if (this.boardGame.GetLevelCount() < 1)
            {
                return true;
            }
        }

        return false;
    }
}
