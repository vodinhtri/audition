﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

enum ButtonDirection
{
    NONE = 0,
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4,
    COUNT = 5
}

enum IntputType
{
    NONE = 0,
    WRONG = 1,
    RIGHT = 2,
    FINISH = 3,
}

enum ResultType
{
    WOW = 0,
    GOOD = 1,
    NICE = 2,
    GREATE = 3,
    PERFECT = 4
}

public class BoardGameController : MonoBehaviour
{
    public float beatSpeed = 80.0f;
    public float beatWait = 22.0f;
    public float cutSceneWait = 12.0f;

    public Transform notificationBoard;    
    public Transform levelText;
    public GameObject hitZone;
    public GameObject beatBoard;
    public Transform buttonBoard;
    public GameObject prefabButtonUp;
    public GameObject prefabButtonDown;
    public GameObject prefabButtonLeft;
    public GameObject prefabButtonRight;

    private int levelNumber;    
    private int levelCount;
    private List<ButtonDirection> listDirection;
    private List<ButtonDirection> listDirectionCheck;
    private List<GameObject> listButton;

    private bool isStartBoard;
    private int stepPrepare = 0;
    private bool waitNewLevel;
    private int waitForMiss;
    private bool runOnce;
    private bool boardGenerated;
    private int animationType;
    // Start is called before the first frame update
    void Start()
    {
        this.levelNumber = 1;
        this.levelCount = 0;
        this.isStartBoard = false;
        this.waitForMiss = 0;

        this.boardGenerated = true;

        this.listButton = new List<GameObject>();
        this.listDirection = new List<ButtonDirection>();
        
        // this.CleanBoard();
        this.GenerateBoard(this.levelNumber);

        this.hitZone.GetComponent<Animator>().SetFloat("speedHitZone", 1.0f / (240.0f / this.beatSpeed));

        this.transform.localScale = Vector3.zero;

        StartCoroutine(WaitCutScene(this.cutSceneWait));
        StartCoroutine(WaitToStart(this.beatWait));
        // this.StartBoard();
    }

    private IEnumerator WaitToStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        this.StartBoard();
    }

    private IEnumerator WaitCutScene(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        this.transform.localScale = Vector3.one;
    }

    // Update is called once per frame
    void Update()
    {
        // this.UpdateBoard(this.CheckInput());
        this.UpdateBoard(0);
    }

    public void CleanBoard()
    {
        this.listDirection.Clear();
        this.listDirectionCheck.Clear();
        foreach (GameObject buttonItem in this.listButton)
        {
            Destroy(buttonItem);
        }
        this.listButton.Clear();
    }

    public void GenerateBoard(int level)
    {        
        this.LevelTextUpdate();
        if (this.waitForMiss > 0)
        {
            this.waitForMiss -= 1;
            return;
        }
        bool isFinishMove = false;
        if (level > 9)
        {
            level = 9;
            isFinishMove = true;
        }

        // Debug.Log("GenerateBoard - level: "+ level);
        for (int i = 0; i < level; i++)
        {
            int button = (int)Random.Range(1, (float)ButtonDirection.COUNT);
            switch ((ButtonDirection)button)
            {
                case ButtonDirection.UP:
                    this.listDirection.Add(ButtonDirection.UP);
                    this.listButton.Add(Instantiate(prefabButtonUp, Vector3.zero, Quaternion.identity, buttonBoard));
                    break;
                case ButtonDirection.DOWN:
                    this.listDirection.Add(ButtonDirection.DOWN);
                    this.listButton.Add(Instantiate(prefabButtonDown, Vector3.zero, Quaternion.identity, buttonBoard));
                    break;
                case ButtonDirection.LEFT:
                    this.listDirection.Add(ButtonDirection.LEFT);
                    this.listButton.Add(Instantiate(prefabButtonLeft, Vector3.zero, Quaternion.identity, buttonBoard));
                    break;
                case ButtonDirection.RIGHT:
                    this.listDirection.Add(ButtonDirection.RIGHT);
                    this.listButton.Add(Instantiate(prefabButtonRight, Vector3.zero, Quaternion.identity, buttonBoard));
                    break;
            }            
        }
        this.listDirectionCheck = new List<ButtonDirection>(this.listDirection);

        if (isFinishMove)
        {
            int posRed = (int)Random.Range(0, level);
            this.listButton[posRed].transform.GetChild(0).gameObject.SetActive(true);
        }
        // listDirection.Clear();
        // Debug.Log(listDirectionCheck.Count);
    }

    public int CheckInput()
    {
        ButtonDirection directionInput = ButtonDirection.NONE;

        if (this.waitForMiss > 0)
        {
            return 0;
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            directionInput = ButtonDirection.UP;
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            directionInput = ButtonDirection.DOWN;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            directionInput = ButtonDirection.LEFT;
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            directionInput = ButtonDirection.RIGHT;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            return 3;
        }

        if (directionInput != ButtonDirection.NONE)
        {
            if (this.listDirectionCheck.Count > 0)
            {
                if (directionInput == listDirectionCheck[0])
                {
                    this.listDirectionCheck.RemoveAt(0);                                        
                    // Debug.Log("CheckInput TRUE - CHECK: " + listDirectionCheck.Count + " - ORIGIN: " + listDirection.Count);
                    // Debug.Log("CheckInput TRUE");
                    return 1;
                }
                else
                {
                    this.listDirectionCheck = new List<ButtonDirection>(this.listDirection);
                    // Debug.Log("CheckInput FALSE - CHECK: " + listDirectionCheck.Count + " - ORIGIN: " + listDirection.Count);
                    return 2;
                }
            }
        }
        return 0;
    }

    public void UpdateBoard(int intputType)
    {
        Slider beatSlider = this.beatBoard.GetComponent<Slider>();
        beatSlider.value += beatSlider.maxValue * this.beatSpeed / 240.0f * Time.deltaTime; //hieubt

        if (beatSlider.value >= beatSlider.maxValue)
        {            
            this.animationType = Random.Range(0, 5);
            beatSlider.value = beatSlider.minValue;
            this.runOnce = false;
        }

        if (beatSlider.value >= 110.0f && !this.runOnce && this.isStartBoard)
        {
            if (this.stepPrepare < 2)
            {                
                this.runOnce = true; // 1-START, 2-321                
                if (this.stepPrepare == 0)
                {
                    this.notificationBoard.GetChild(5).gameObject.SetActive(false);
                    this.notificationBoard.GetChild(6).gameObject.SetActive(true);
                }
                if (this.stepPrepare == 1)
                {
                    this.notificationBoard.GetChild(6).gameObject.SetActive(false);
                    this.notificationBoard.GetChild(7).gameObject.SetActive(true);

                    Animator animatorCountdown = this.notificationBoard.GetChild(7).GetComponent<Animator>();
                    animatorCountdown.SetFloat("speedCountdown", 1.0f / (240.0f / this.beatSpeed));
                }
            }
            this.stepPrepare++;
        }

        if (this.stepPrepare >= 2)
        {
            switch (this.CheckInput())
            {
                case 1:
                    GameObject button = this.listButton[this.listDirection.Count - this.listDirectionCheck.Count - 1];
                    Image doneImage = button.transform.Find("Done").GetComponent<Image>();
                    doneImage.fillAmount += 1.0f;// * Time.deltaTime;
                    break;
                case 2:
                    foreach (GameObject buttonItem in this.listButton)
                    {
                        Image doneImageItem = buttonItem.transform.Find("Done").GetComponent<Image>();
                        doneImageItem.fillAmount -= 1.0f;// * Time.deltaTime;
                    }
                    break;
                case 3:
                    if (!this.runOnce)
                    {
                        int finishResult = this.FinishResult(beatSlider.value);
                        if (this.listDirectionCheck.Count > 0)
                        {
                            this.ShowNotification(0);
                        }
                        else
                        {
                            this.ShowNotification(finishResult);
                        }
                        // if(finishResult != 0)
                        // {
                            this.runOnce = true;
                            this.levelCount++;
                            this.GetNewLevel();
                        // }
                    }
                    break;
                default:
                    if (!this.runOnce)
                    {
                        if (this.listDirection.Count > 0)
                        {
                            if (beatSlider.value >= 130.0f && this.listDirectionCheck.Count > this.listDirection.Count / 3)
                            {
                                this.ShowNotification(0);
                                this.runOnce = true;
                                this.levelCount++;
                                this.GetNewLevel();
                            } else if (beatSlider.value >= 145.0f)
                            {
                                this.ShowNotification(0);
                                this.runOnce = true;
                                this.levelCount++;
                                this.GetNewLevel();
                            }
                        }
                        else
                        {
                            if (beatSlider.value >= 110.0f)
                            {
                                this.runOnce = true;
                                this.levelCount++;
                                this.GetNewLevel();
                            }
                        }
                    }
                    break;
            }
        }

        //Debug
        // if (!this.isStartBoard)
        // {
        //     if (Input.GetKeyUp(KeyCode.Space))
        //     {
        //         this.StartBoard();
        //     }
        // }        
        
        // if (beatSlider.value >= 145.0f && !this.waitNewLevel && this.isStartBoard)
        // {
        //     this.waitNewLevel = true;
        //     if (this.stepPrepare < 2)
        //     {
        //         this.stepPrepare++;
        //     }
        //     // else
        //     // {
        //     //     if (this.listButton.Count > 0)
        //     //     {
        //     //         this.ShowNotification(0);
        //     //     }
        //     //     Debug.Log("GetNewLevel: 3");
        //     //     this.levelCount++;
        //     //     this.GetNewLevel();
        //     // }
        //     Debug.Log("this.stepPrepare: " + this.stepPrepare);
        // }

        // if (beatSlider.value >= 110.0f && this.stepPrepare < 2 && !this.waitNewLevel)
        // {
        //     this.waitNewLevel = true;
        //     foreach (Transform item in this.notificationBoard)
        //     {
        //         item.gameObject.SetActive(false);
        //     }
        //     switch(this.stepPrepare)
        //     {
        //         case 0:
        //             this.notificationBoard.Find("START").gameObject.SetActive(true);
        //             // Debug.Log("START");
        //             break;
        //         case 1:
        //             // Debug.Log("321");
        //             break;
        //     }
        // }
    }

    public int FinishResult(float sliderValue)
    {
        switch (sliderValue)
        {
            case float n when (n < 75 || n > 145):       
                return 0;
            case float n when ((n >= 75 && n < 90) || (n > 130 && n <= 145)):
                return 1;
            case float n when ((n >= 90 && n < 100) || (n > 120 && n <= 130)):
                return 2;
            case float n when ((n >= 100 && n < 107) || (n > 113 && n <= 120)):
                return 3;
            case float n when (n >= 107 && n <= 113):
                return 4;
            default:
                return 0;
        }
    }

    public void GetNewLevel()
    {
        if (this.levelNumber < 6)
        {
            if (this.levelCount >= this.levelNumber)
            {
                this.levelNumber++;
                this.levelCount = 0;
            }

            this.CleanBoard();
            this.GenerateBoard(this.levelNumber);
        } 
        else if (this.levelNumber < 10)
        {
            if (this.levelCount >= 6 * 2)
            {
                this.levelNumber++;
                this.levelCount = 0;
            }

            if (this.levelCount % 2 == 1)
            {
                this.CleanBoard();
            }
            else
            {
                this.CleanBoard();
                this.GenerateBoard(this.levelNumber);
            }
        }
        else if (this.levelNumber == 10)
        {
            this.CleanBoard();
            if (this.levelCount < 1)
            {
                this.GenerateBoard(this.levelNumber);
                this.levelCount++;
            }
            if (this.levelCount > 3)
            {
                this.levelNumber++;
            }
        }
        else
        {
            this.levelNumber = 6;
            this.levelCount = 0;

            this.CleanBoard();
            this.GenerateBoard(this.levelNumber);
        }
    }

    public void StartBoard()
    {
        this.isStartBoard = true;
    }

    public void ShowNotification(int finishResult)
    {
        switch (finishResult)
        {
            case 0:
                this.notificationBoard.GetChild(0).gameObject.SetActive(false);
                this.notificationBoard.GetChild(0).gameObject.SetActive(true);
                this.waitForMiss = 1;//hieubt
                AnimCharManager.instance.SetMiss("char1");
                break;
            case 1:
                this.notificationBoard.GetChild(1).gameObject.SetActive(false);
                this.notificationBoard.GetChild(1).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(0, 20.0f);
                break;
            case 2:
                this.notificationBoard.GetChild(2).gameObject.SetActive(false);
                this.notificationBoard.GetChild(2).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(0, 50.0f);
                break;
            case 3:
                this.notificationBoard.GetChild(3).gameObject.SetActive(false);
                this.notificationBoard.GetChild(3).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(0, 80.0f);
                break;
            case 4:
                this.notificationBoard.GetChild(4).gameObject.SetActive(false);
                this.notificationBoard.GetChild(4).gameObject.SetActive(true);
                LbController.instance.SetScoreCharacter(0, 100.0f);
                break;
        }

        if (finishResult != 0)
        {
            if (this.levelNumber <= 9)
            {
                // AnimCharManager.instance.SetDance((int) Random.Range(1, 6));
                AnimCharManager.instance.SetDanceSingle("char1", this.animationType);
            }
            else
            {
                // AnimCharManager.instance.SetFinishDance();
                AnimCharManager.instance.SetFinishDanceSingle("char1", this.animationType % 2);
            }
        }
    }

    public void LevelTextUpdate()
    {
        GameObject finishMoveText = this.levelText.parent.GetChild(0).gameObject;
        if (this.levelNumber < 10) //lv 1-9
        {
            finishMoveText.SetActive(false);
            this.levelText.gameObject.SetActive(true);
            foreach (Transform item in this.levelText)
            {
                item.gameObject.SetActive(false);
            }
            this.levelText.GetChild(this.levelNumber - 1).gameObject.SetActive(true);
        }
        else //finish move
        {
            finishMoveText.SetActive(true);
            this.levelText.gameObject.SetActive(false);
        }

    }

    public int GetLevelNumber()
    {
        return this.levelNumber;
    }

    public int GetLevelCount()
    {
        return this.levelCount;
    }

    public Slider GetBeatSlide()
    {
        return this.beatBoard.GetComponent<Slider>();
    }

    public int GetStepPrepare()
    {
        return this.stepPrepare;
    }

    public int GetAnimationType()
    {
        return this.animationType;
    }
}
