﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimCharManager : MonoBehaviour
{
    public static AnimCharManager instance;
    public GameObject spotLight;
    public GameObject directLight;
    public GameObject nameChar_1;
    public GameObject nameChar_2;
    public GameObject nameChar_3;
    public GameObject arrow;
    public GameObject character_1;
    public GameObject character_2;
    public GameObject character_3;

    private float speed = 10f;
    private Animator anim_1;
    private Animator anim_2;
    private Animator anim_3;
    private GameObject objMove1;
    private GameObject objMove2;
    private GameObject charTop;
    private Vector3 startMove;
    private Vector3 endMove;
    private bool isMoving = false;
    private float startTime;
    private float journeyLength;
    private bool isShowName = false;
    private bool isGameOver = false;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        anim_1 = character_1.GetComponent<Animator>();
        anim_2 = character_2.GetComponent<Animator>();
        anim_3 = character_3.GetComponent<Animator>();

        charTop = character_1;
        // gamePlay.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!isGameOver){
            if(isMoving){
                float distCovered = (Time.time - startTime) * speed;
                float fractionOfJourney = distCovered / journeyLength;
                objMove1.transform.position = Vector3.Lerp(startMove, endMove, fractionOfJourney);
                objMove2.transform.position = Vector3.Lerp(endMove, startMove, fractionOfJourney);
                if(objMove1.transform.position == endMove && objMove2.transform.position == startMove){
                    isMoving = false;
                    RsPos(character_1);
                }
            }
            if(!isShowName && IsFinishCutScene()){
                isShowName = true;
                ShowName();
            }
            if(SoundController.instance.IsFinish()){
                GameOver();
            }
        }
    }
    public void SetDance( int type = 1){
        if(type <= 0 || type > 5) type = 1;
        anim_1.SetBool("isDance", true);
        anim_2.SetBool("isDance", true);
        anim_3.SetBool("isDance", true);
        anim_1.SetBool("isFinishDance", false);
        anim_2.SetBool("isFinishDance", false);
        anim_3.SetBool("isFinishDance", false);
        ChangeState(anim_1);
        ChangeState(anim_2);
        ChangeState(anim_3);
        anim_1.SetInteger("dancingType", type);
        anim_2.SetInteger("dancingType", type);
        anim_3.SetInteger("dancingType", type);

    }
    public void SetDanceSingle(  string characterPlay, int type = 1){
        Animator animator;

        if(characterPlay == "char1") animator = anim_1;
        else if(characterPlay == "char2") animator = anim_2;
        else animator = anim_3;

        if(type <= 0 || type > 5) type = 1;
        animator.SetBool("isDance", true);
        animator.SetBool("isFinishDance", false);
        ChangeState(animator);
        animator.SetInteger("dancingType", type);

    }
    public void SetMiss( string anim){
        switch(anim){
            case "char1" :
            {
                anim_1.SetBool("isDance", false);
                ChangeState(anim_1);
                RsPos(character_1);
                break;
            }
            case "char2":
            {
                anim_2.SetBool("isDance", false);
                ChangeState(anim_2);
                RsPos(character_2);
                break;
            }
            case "char3":
            {
                anim_3.SetBool("isDance", false);
                ChangeState(anim_3);
                RsPos(character_3);
                break;
            }
        }
    }
    public void SetFinishDance(int type = 2){
        RsPos(character_1);
        RsPos(character_2);
        RsPos(character_3);
        // ResetPosition(character_2, old2);
        // ResetPosition(character_3, old3);

        // type = Random.Range(1, 3);
        type = 1;
        if(type <= 0 || type > 3) type = 1;
        anim_1.SetBool("isDance", true);
        anim_2.SetBool("isDance", true);
        anim_3.SetBool("isDance", true);
        anim_1.SetBool("isFinishDance", true);
        anim_2.SetBool("isFinishDance", true);
        anim_3.SetBool("isFinishDance", true);
        ChangeState(anim_1);
        ChangeState(anim_2);
        ChangeState(anim_3);
        anim_1.SetInteger("finishType", type);
        anim_2.SetInteger("finishType", type);
        anim_3.SetInteger("finishType", type);
    }
    public void SetFinishDanceSingle(string characterPlay, int type = 2){

        Animator animator;

        if(characterPlay == "char1") animator = anim_1;
        else if(characterPlay == "char2") animator = anim_2;
        else animator = anim_3;

        RsPos(character_1);
        RsPos(character_2);
        RsPos(character_3);

        type = Random.Range(1, 3);
        if(type <= 0 || type > 3) type = 1;
        animator.SetBool("isDance", true);
        animator.SetBool("isFinishDance", true);
        ChangeState(animator);
        animator.SetInteger("finishType", type);
    }

    public void MoveCharacterUp(int charIndexRank1){ // character 1 : 1, character 2 : 2,..
        if(isMoving == true) return;

        GameObject char1;

        if(charIndexRank1 == 1) char1 = character_1;
        else if(charIndexRank1 == 2) char1 = character_2;
        else char1 = character_3;

        if(char1 == charTop) return;

        startTime = Time.time;
        objMove1 = char1.transform.parent.gameObject;
        objMove2 = charTop.transform.parent.gameObject;

        charTop = char1;

        startMove = objMove1.transform.position;
        endMove = objMove2.transform.position;
        isMoving = true;
        journeyLength = Vector3.Distance(startMove, endMove);
    }


//------PRIVATE FUNCTION
    private void GameOver(){
        isGameOver = true;
        // LbController.instance.topPlayer == 1
        SetMiss("char1");
        SetMiss("char2");
        SetMiss("char3");
        charTop.GetComponent<Animator>().SetTrigger("isWin");
    }

    private void RsPos(GameObject obj){
        StartCoroutine(WaitReset(obj));
    }
    private IEnumerator WaitReset(GameObject obj){
        yield return new WaitForSeconds(0.1f);
        ResetPosition(obj);
    }
    private void ChangeState(Animator anim){
        anim.SetTrigger("changeState");
    }
    private void ResetPosition(GameObject objTrans){
        objTrans.transform.localPosition  = new Vector3(0f, 0f, 0f);
        objTrans.transform.GetChild(1).localPosition  = new Vector3(0f, 0f, 0f);

        // objTrans.transform.position  = new Vector3(0f, 0f, 0f);
    }
    private bool IsFinishCutScene(){
        if(TimelineController.instance.isFinish) return true;
        return false;
    }
    private void ShowName(){
        directLight.SetActive(true); 
        nameChar_1.SetActive(true);
        nameChar_2.SetActive(true);
        nameChar_3.SetActive(true);
        arrow.SetActive(true);
    }
}
