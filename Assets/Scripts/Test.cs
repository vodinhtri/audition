﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    public int type = 1;
    public bool charMiss1 = false;
    public bool charMiss2 = false;
    public bool charMiss3 = false;
    public bool setFinish = false;
    public bool setDance = false;
    public int char1 = 1;
    public bool setMove = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(setFinish){
            setFinish = false;
            AnimCharManager.instance.SetFinishDance();
        }
        if(charMiss1){
            charMiss1 = false;
            AnimCharManager.instance.SetMiss("char1");
        }
        if(charMiss2){
            charMiss2 = false;
            AnimCharManager.instance.SetMiss("char2");
        }
        if(charMiss3){
            charMiss3 = false;
            AnimCharManager.instance.SetMiss("char3");
        }
        if(setDance){
            setDance = false;
            AnimCharManager.instance.SetDance(type);
        }
        if(setMove){
            setMove = false;
            AnimCharManager.instance.MoveCharacterUp(char1);
        }
    }
}
